define network::general($hostname = $name, $domain, $nameservers = []) {

    include network

    file { '/etc/resolv.conf' :
	ensure => file,
	owner => 'root',
        group => 'root',
        mode => 644,
        content => template('network/resolv.conf.erb'),
    }

    case $::osfamily {

	'RedHat': {
		augeas { 'hostname' :
			context => "/files/etc/sysconfig/network",
			changes => "set HOSTNAME $hostname.$domain",
		}
	}

        'Debian': {
        }

        default: {
            fail("Unsupported osfamily: ${::osfamily} operatingsystem: ${::operatingsystem}, module ${module_name} only support osfamily RedHat and Debian")
        }

    }
}
