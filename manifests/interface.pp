define network::interface($dev = $name, $ip, $mask, $gateway = false) {

    include network

    case $::osfamily {

	'RedHat': {
		network::interface::redhat { "iface-$dev" :
			dev => $dev,
			ip => $ip,
			mask => $mask,
		}
		if($gateway) {
			augeas { 'gateway' :
				context => "/files/etc/sysconfig/network",
				changes => "set GATEWAY $gateway",
				notify => Service['network']
			}
		}
	}

        'Debian': {
            network::interface::debian { "iface-$dev" :
                dev => $dev,
                ip => $ip,
                mask => $mask,
                gateway => $gateway
            }
        }

        default: {
            fail("Unsupported osfamily: ${::osfamily} operatingsystem: ${::operatingsystem}, module ${module_name} only support osfamily RedHat and Debian")
        }

    }
}

define network::interface::redhat($dev = $name, $ip, $mask) {
	
	file { "/etc/sysconfig/network-scripts/ifcfg-$dev" :
		ensure => file,
		owner => 'root',
		group => 'root',
		mode => 644,
		content => template('network/redhat/ifcfg-dev.erb'),
		notify => Service['network']
	}

}

define network::interface::debian($dev = $name, $ip, $mask, $gateway) {
    fail("Setting Debian interface")
}