
class network {

    case $::osfamily {

	'RedHat': {

		service { 'network' :
			name => network,
			hasstatus => true,
			hasrestart => true,
			ensure => running
		}

		augeas { 'network' :
			context => "/files/etc/sysconfig/network",
			changes => [
				"set NETWORKING yes",
				"set NETWORKING_IPV6 yes",
			],
		}


	}

        'Debian': {
        }

        default: {
            fail("Unsupported osfamily: ${::osfamily} operatingsystem: ${::operatingsystem}, module ${module_name} only support osfamily RedHat and Debian")
        }

    }


}
